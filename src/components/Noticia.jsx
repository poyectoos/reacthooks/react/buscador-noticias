import React from 'react';
import PropTypes from 'prop-types';

const Noticia = ({ noticia }) => {

  const { source, author, title, description, url, urlToImage } = noticia;


  return (
    <div className="col s12 m6 l4">
      <div className="card">
        <div className="card-image">
          <img src={
            urlToImage
             ?
            urlToImage
             :
            'https://octodex.github.com/images/yaktocat.png'
          } alt={ title } />
          <span className="card-title truncate">
            <div>
              <small>{ author }</small>
            </div>
            <div>
              { source.name }
            </div>
          </span>
        </div>
        <div className="card-content">
          <h5>{ title ? title.substr(0,50) : null }...</h5>
          <p>{ description ? description.substr(0,150) : null }...</p>
        </div>
        <div className="card-action">
          <a
            href={url}
            target="_blank"
            rel="noopener noreferrer"
            className="waves-effect waves-light btn"
          >
            Ver noticia completa
          </a>
        </div>
      </div>
    </div>
  );
};

Noticia.propTypes = {
  noticia: PropTypes.object.isRequired
};

export default Noticia;