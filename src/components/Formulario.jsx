import PropTypes from 'prop-types';

import styles from './Formulario.module.css';
import useSelect from '../hooks/useSelect';

const Formulario = ({ actualizarCategoria }) => {

  const OPCIONES = [
    { value: 'business', label: 'Negocios' },
    { value: 'entertainment', label: 'Entretenimiento' },
    { value: 'general', label: 'General' },
    { value: 'health', label: 'Salud' },
    { value: 'science', label: 'Ciencia' },
    { value: 'sports', label: 'Deportes' },
    { value: 'technology', label: 'Tecnologia' },
  ]

  const [ categoria, SelectCategorias ] = useSelect('general', OPCIONES);


  const handleSubmit = e => {
    e.preventDefault();
    actualizarCategoria(categoria);
  }

  return (
    <div className={`row ${styles.buscador}`}>
      <div className="col s12 m8 offset-m2">
        <form
          onSubmit={ handleSubmit }
        >
          <h2 className={styles.heading}>Noticias destacadas</h2>
          <div className="input-field col s12">
            <SelectCategorias />
          </div>
          <div className="input-field col s12">
            <button
              className={ `btn btn-large ${styles['btn-block']} amber darken-2` }
            >
              Buscar
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

Formulario.propTypes = {
  actualizarCategoria: PropTypes.func.isRequired
};

export default Formulario;