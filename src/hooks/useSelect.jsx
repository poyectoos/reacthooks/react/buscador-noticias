import { Fragment, useState } from 'react';
import PropTypes from 'prop-types';

const useSelect = ( inicial, opciones ) => {

  const [ state, updateState ] = useState(inicial);

  const handleChange = e => {
    updateState(e.target.value);
  } 

  const Select = () => (
    <Fragment>
      <select
        name=""
        id=""
        className="browser-default"
        value={ state }
        onChange={ handleChange }
      >
        <option value="">Selecciona</option>
        {
          opciones.map(opcion => (
            <option key={opcion.value} value={opcion.value}>{ opcion.label }</option>
          ))
        }
      </select>
      <label htmlFor=""></label>
    </Fragment>
  );


  return [state, Select];
};

useSelect.propTypes = {
  inicial: PropTypes.string.isRequired,
  opciones: PropTypes.array.isRequired
};

export default useSelect;