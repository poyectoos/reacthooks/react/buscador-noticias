import { Fragment, useState, useEffect } from 'react';

import Header from './components/Header';
import Formulario from './components/Formulario';
import Noticias from './components/Noticias';
import Spinner from './components/Spinner';

function App() {

  const [ categoria, actualizarCategoria ] = useState('');
  const [ noticias, actualizarNoticias ] = useState([]);


  useEffect(() => {
    const contultar = async () => {
      const KEY = '86344b2986b54f03a86d510da998fc87';
      const country = 'mx';
  
      const URI = `https://newsapi.org/v2/top-headlines?country=${country}&category=${categoria}&apiKey=${KEY}`;
  
      actualizarNoticias([]);
      const respuesta = await fetch(URI);
      const data = await respuesta.json();
      //const data = await axios.get(URI);
      actualizarNoticias(data.articles);
    }
    contultar();
  }, [categoria]);
  
  return (
    <Fragment>
      <Header titulo="Buscador de noticias" />
      <div className="container white">
        <Formulario
          actualizarCategoria={ actualizarCategoria }
        />
        {
          noticias
            ?
          <Noticias
          noticias={ noticias }
          />
            :
          <Spinner />
        }
      </div>
    </Fragment>
  );
}

export default App;
